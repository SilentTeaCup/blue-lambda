# Blue lambda zsh theme

A very simple zsh theme with no dependencies on oh-my-zsh.

## Features

- git prompt
  - current branch
  - marker for incommited changes
  - difference between remote (commits ahead/behind)
- current virtual environment 
