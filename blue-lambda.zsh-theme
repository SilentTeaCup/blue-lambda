#!/usr/bin/env zsh
# blue-lambda.zsh-theme
# Repo: https://gitlab.com/SilentTeaCup/blue-lambda

##################
# Colors
##################

# primary color
ZSH_THEME_BLUELAMBDA_CPRIMARY='%F{75}'
# secondary color (git branch name and remote status)
ZSH_THEME_BLUELAMBDA_CSECONDARY='%F{78}'
# git changes marker color
ZSH_THEME_BLUELAMBDA_CDIRTY='%F{214}'
# color for additional info (host and user name and venv)
ZSH_THEME_BLUELAMBDA_CCHAR='%F{105}'
# color for additional info (host and user name and venv)
ZSH_THEME_BLUELAMBDA_CINFO='%F{248}'

##################
# Prompt
##################

function +vi-git-remote() {
  # if there is an ongoing action remote info is 
  # not needed.
  [ -n "$hook_com[misc]" ] && return 0

  local c1="$ZSH_THEME_BLUELAMBDA_CPRIMARY"
  local c2="$ZSH_THEME_BLUELAMBDA_CSECONDARY"
  local ahead behind
  local -a gitstatus

  ahead=$(command git rev-list --count "${hook_com[branch]}"@{upstream}..HEAD 2>/dev/null)
  (( ahead )) && gitstatus+=( "${ahead}↑" )

  behind=$(command git rev-list --count HEAD.."${hook_com[branch]}"@{upstream} 2>/dev/null)
  (( behind )) && gitstatus+=( "${behind}↓" )

  if [ -z "$gitstatus" ]; then
    gitstatus='='
  fi

  hook_com[misc]=${(j::)gitstatus}
}

prompt_git() {
  vcs_info
  print -n "${vcs_info_msg_0_}"
}

prompt_sep() {
  printf "-%.0s" {1..$COLUMNS}
}

prompt_dir() {
  local c="$ZSH_THEME_BLUELAMBDA_CPRIMARY"
  print -n "$c%~ "
}

prompt_char() {
  local c="$ZSH_THEME_BLUELAMBDA_CCHAR"
  print "\n%(?.$c.%F{red})%(!.#.λ)%f "
}

prompt_build() {
  prompt_sep
  prompt_dir
  prompt_git
  prompt_char
}

rprompt_build() {
  print -n "$ZSH_THEME_BLUELAMBDA_CINFO"
  if [ -n "$VIRTUAL_ENV" ]; then
    print -n "(%B$VIRTUAL_ENV%b) "
  fi
  print -n "%n@%m%f"
}

##################
# Init
##################

function() {
  local c1="$ZSH_THEME_BLUELAMBDA_CPRIMARY"
  local c2="$ZSH_THEME_BLUELAMBDA_CSECONDARY"
  local cm="$ZSH_THEME_BLUELAMBDA_CDIRTY"
  
  # disable prompt modification
  export VIRTUAL_ENV_DISABLE_PROMPT=1

  setopt PROMPT_SUBST

  # git format
  autoload -Uz vcs_info
  zstyle ':vcs_info:*' enable git
  zstyle ':vcs_info:git*' unstagedstr " $cm●"
  zstyle ':vcs_info:git*' check-for-changes true
  zstyle ':vcs_info:git*' patch-format "%n/%a"
  zstyle ':vcs_info:git*+set-message:*' hooks git-remote
  zstyle ':vcs_info:git*' formats "$c1($c2%b%u$c1)[$c2%m$c1]%f"
  zstyle ':vcs_info:git*' actionformats "$c1($c2%b|%a%u$c1)[$c2%m$c1]%f"

  # primary prompt
  PROMPT='$(prompt_build)'

  # right prompt
  RPROMPT='$(rprompt_build)'
}
